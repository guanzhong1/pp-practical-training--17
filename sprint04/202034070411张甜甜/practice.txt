CREATE EXTERNAL TABLE dim_source_full
(
    `id`          STRING COMMENT '编号',
    `source_site` STRING COMMENT '来源'
) COMMENT '来源维度表'
    PARTITIONED BY (`dt` STRING)
    STORED AS ORC
    LOCATION '/warehouse/edu/dim/dim_source_full/'
    TBLPROPERTIES ('orc.compress' = 'snappy');

INSERT OVERWRITE TABLE edu.dim_source_full PARTITION (dt = '2023-07-01')
SELECT id,
       source_site
FROM edu.ods_base_source_full obsf
WHERE dt = '2023-07-01';
