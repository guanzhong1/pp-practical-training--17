//同步用户留存率表

 graph TB
 HIVE中ads_user_user_retention --> MySQL中ads_user_user_retention

ADS_USER_USER_RETENTION

drop table if exists ads_user_user_retention;
create table ads_user_user_retention
(
    `dt`              datetime comment '统计日期',
    `create_date`     varchar(255) comment '用户新增日期',
    `retention_day`   int comment '截至当前日期留存天数',
    `retention_count` bigint comment '留存用户数量',
    `new_user_count`  bigint comment '新增用户数量',
    `retention_rate`  decimal(16, 2) comment '留存率',
    primary key (`dt`, `create_date`, `retention_day`)
) comment '用户留存率';


{
    "job": {
        "content": [
            {
                "reader": {
                    "name": "hdfsreader",
                    "parameter": {
                        "column": [
                            "*"
                        ],
                        "defaultFS": "hdfs://niitmaster:8020",
                        "encoding": "UTF-8",
                        "fieldDelimiter": "\t",
                        "fileType": "text",
                        "nullFormat": "\\N",
                        "path": "**${exportdir}**"
                    }
                },
                "writer": {
                    "name": "mysqlwriter",
                    "parameter": {
                        "column": [
                            "dt",
                            "create_date",
                            "retention_day",
                            "retention_count",
                            "new_user_count",
                            "retention_rate"
                        ],
                        "connection": [
                            {
                                "jdbcUrl": "jdbc:mysql://niitmaster:3306/edu_report?useUnicode=true&characterEncoding=utf-8",
                                "table": [
                                    "ads_user_user_retention"
                                ]
                            }
                        ],
                        "password": "000000",
                        "username": "root",
                        "writeMode": "replace"
                    }
                }
            }
        ],
        "setting": {
            "errorLimit": {
                "percentage": 0.02,
                "record": 0
            },
            "speed": {
                "channel": 3
            }
        }
    }
}

