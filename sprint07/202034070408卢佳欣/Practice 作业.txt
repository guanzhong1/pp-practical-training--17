//用户留存统计分析
    graph TB
    ods_user_info_inc --> dwd_user_register_inc --> ads_user_user_retention
    ods_user_info_inc --> dim_user_zip --> dws_user_user_login_td  --> ads_user_user_retention
    ods_log_inc --> dwd_user_login_inc --> dws_user_user_login_td
    graph TB
    dwd_user_register_inc --> Temp_Table
    dws_user_user_login_td -->  Temp_Table
    Temp_Table --> ads_user_user_retention
    ads_user_user_retention-->ads_user_user_retention
DROP TABLE IF EXISTS ads_user_user_retention;
create external table ads_user_user_retention
(
    dt              string comment '统计日期',
    create_date     string comment '用户新增日期',
    retention_day   int comment '截至当前日期留存天数',
    retention_count bigint comment '留存用户数量',
    new_user_count  bigint comment '新增用户数量',
    retention_rate  decimal(16, 2) comment '留存率'
) COMMENT '用户留存率'
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
    LOCATION '/warehouse/edu/ads/ads_user_user_retention/';
insert overwrite table edu2077.ads_user_user_retention
select dt,
       create_date,
       retention_day,
       retention_count,
       new_user_count,
       retention_rate
from edu2077.ads_user_user_retention
union
select '2023-07-01'                                             dt,
       register_date,
       retention_day,
       sum(if(login_last_date = '2023-07-01', 1, 0))            retention_count,
       count(*)                                                 new_user_count,
       sum(if(login_last_date = '2023-07-01', 1, 0)) / count(*) retention_rate
from (
         select user_id,
                register_date,
                retention_day
         from edu2077.dwd_user_register_inc
                  lateral view explode(array(1, 2, 3, 4, 5, 6, 7)) tmp as retention_day
         where dt = date_add('2023-07-01', -retention_day)
     ) previous
         left join
     (
         select user_id,
             login_last_date
         from edu2077.dws_user_user_login_td
         where dt = '2023-07-01'
     ) today
     on today.user_id = previous.user_id
group by register_date,
         retention_day;
