习6 创建课程信息表（全量）
DROP TABLE IF EXISTS `ods_course_info_full`;
CREATE EXTERNAL TABLE `ods_course_info_full`
(
    `id`               STRING COMMENT '编号',
    `course_name`      STRING COMMENT '课程名称',
    `course_slogan`    STRING COMMENT '课程标语',
    `course_cover_url` STRING COMMENT '课程封面',
    `subject_id`       STRING COMMENT '学科id',
    `teacher`          STRING COMMENT '讲师名称',
    `publisher_id`     STRING COMMENT '发布者id',
    `chapter_num`      BIGINT COMMENT '章节数',
    `origin_price`     DECIMAL(16, 2) COMMENT '价格',
    `reduce_amount`    DECIMAL(16, 2) COMMENT '优惠金额',
    `actual_price`     DECIMAL(16, 2) COMMENT '实际价格',
    `course_introduce` STRING COMMENT '课程介绍',
    `create_time`      STRING COMMENT '创建时间',
    `deleted`          STRING COMMENT '是否删除',
    `update_time`      STRING COMMENT '更新时间'
) COMMENT '课程信息全量表'
    PARTITIONED BY (`dt` STRING)
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        NULL DEFINED AS ''
    LOCATION '/warehouse/edu/ods/ods_course_info_full/';


LOAD DATA INPATH "/origin_data/edu/db/course_info_full/2023-07-01"
    INTO TABLE ods_course_info_full
    PARTITION (dt = '2023-07-01');