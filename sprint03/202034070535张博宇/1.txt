练习3 创建科目信息表（全量）
DROP TABLE IF EXISTS `ods_base_subject_info_full`;
CREATE EXTERNAL TABLE `ods_base_subject_info_full`
(
    `id`           STRING COMMENT '编号',
    `subject_name` STRING COMMENT '科目名称',
    `category_id`  STRING COMMENT '分类id',
    `create_time`  STRING COMMENT '创建时间',
    `update_time`  STRING COMMENT '更新时间',
    `deleted`      STRING COMMENT '是否删除'
) COMMENT '科目信息全量表'
    PARTITIONED BY (`dt` STRING)
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        NULL DEFINED AS ''
    LOCATION '/warehouse/edu/ods/ods_base_subject_info_full/';

LOAD DATA INPATH "/origin_data/edu/db/base_subject_info_full/2023-07-01"
    INTO TABLE ods_base_subject_info_full
    PARTITION (dt = '2023-07-01');